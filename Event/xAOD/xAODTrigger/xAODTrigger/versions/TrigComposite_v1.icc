// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: TrigComposite_v1.icc 784388 2016-11-15 17:08:58Z tamartin $
#ifndef XAODTRIGGER_VERSIONS_TRIGCOMPOSITE_V1_ICC
#define XAODTRIGGER_VERSIONS_TRIGCOMPOSITE_V1_ICC

// System include(s):
#include <iostream>
#include <stdexcept>

// xAOD include(s):
#include "AthContainers/DataVector.h"
#include "AthContainers/normalizedTypeinfoName.h"
#include "xAODCore/CLASS_DEF.h"

namespace xAOD {

   template< typename TYPE >
   bool TrigComposite_v1::hasDetail( const std::string& name ) const {

      // Object used to check for the existence of an object:
      Accessor< TYPE > acc( name );

      // Use the Accessor object for the check:
      return acc.isAvailable( *this );
   }

   template<>     
   bool TrigComposite_v1::hasDetail<unsigned int>( const std::string& name ) const;

   template<>     
   bool TrigComposite_v1::hasDetail<std::vector<unsigned int>>( const std::string& name ) const;


  template< typename TYPE >
  TYPE TrigComposite_v1::getDetail( const std::string& name ) const {
   TYPE temp;
   if ( getDetail(name, temp) == false ) 
     throw std::runtime_error( "xAOD::TrigComposite::getDetail<TYPE>("+name+") encountered missing detail"); 
   return temp; // RVO
  }

  template<typename TYPE>
  std::pair<bool, TYPE> TrigComposite_v1::returnDetail( const std::string& name ) const {
    TYPE temp;
    bool result = getDetail(name, temp);
    return std::make_pair(result, temp);
  }
  
  template< class CONTAINER >
  bool
  TrigComposite_v1::setObjectLink( const std::string& name,
                                   const ElementLink< CONTAINER >& link ) {
    
    // Check link has valid persistent state, i.e. hash key is not
    // zero, otherwise attempting to access its string key will seg
    // fault later, e.g. in remapping.
    if( link.key() == 0 ) {
      std::cerr << "xAOD::TrigComposite_v1::setObjectLink ERROR "
                << "link has invalid key hash of zero" << std::endl;
      return false;
    }
       
    if( ! link.isValid() ) {
      std::cerr << "xAOD::TrigComposite_v1::setObjectLink ERROR "
                << "link is not valid" << std::endl;
      return false;
    }
       
    // Do different things depending on whether this variable already
    // exists or not:
    if( hasObjectLink( name ) ) {
      // Find the right object:
      const std::vector< std::string >& names = linkColNames();
      for( size_t i = 0; i < names.size(); ++i ) {
        if( names[ i ] != name ) continue;
        // Extract the information out of the ElementLink:
        linkColKeysNC()[ i ] = link.key();
        linkColIndicesNC()[ i ] = link.index();
        linkColClidsNC()[ i ] = ClassID_traits< CONTAINER >::ID();
        // We're done:
        return true;
      }
      // Some error happened...
      std::cerr << "xAOD::TrigComposite_v1::setObjectLink ERROR Internal "
                << "logic error found" << std::endl;
      return false;
    } else {
      // Add a new object:
      linkColNamesNC().push_back( name );
      linkColKeysNC().push_back( link.key() );
      linkColIndicesNC().push_back( link.index() );
      linkColClidsNC().push_back( ClassID_traits< CONTAINER >::ID() );
      // And we're done:
      return true;
    }
  }
  
   template< class CONTAINER >
   ElementLink< CONTAINER >
   TrigComposite_v1::objectLink( const std::string& name ) const {

      // Find the right object:
      const std::vector< std::string >& names = linkColNames();
      for( size_t i = 0; i < names.size(); ++i ) {
         if( names[ i ] != name ) continue;
         // Check that it is of the right type:
         if( linkColClids()[ i ] != ClassID_traits< CONTAINER >::ID() ) {
            const std::string typeName =
               SG::normalizedTypeinfoName( typeid( CONTAINER ) );
            throw std::runtime_error( "xAOD::TrigComposite::objectLink: "
                                      "Wrong type (" + typeName + ") requested "
                                      "for name \"" + name + "\"" );
         }
         // Construct the link:
         return ElementLink< CONTAINER >( linkColKeys()[ i ],
                                          linkColIndices()[ i ] );
      }

      // We didn't find the link. :-(
      throw std::runtime_error( "xAOD::TrigComposite::objectLink: No link "
                                "name \"" + name + "\" found" );
      return ElementLink< CONTAINER >();
   }

   template< class OBJECT >
   const OBJECT* TrigComposite_v1::object( const std::string& name ) const {

      // Check if the link exists:
      if( ! hasObjectLink( name ) ) {
         return 0;
      }

      // Now look for it:
      const std::vector< std::string >& names = linkColNames();
      for( size_t i = 0; i < names.size(); ++i ) {
         if( names[ i ] != name ) continue;
         // Check that it is of the right type:
         if( linkColClids()[ i ] !=
             ClassID_traits< DataVector< OBJECT > >::ID() ) {
            const std::string typeName =
               SG::normalizedTypeinfoName( typeid( OBJECT ) );
            std::cerr << "xAOD::TrigComposite_v1::object WARNING Wrong type ("
                      << typeName << ") requested for CLID "
                      << linkColClids()[ i ] << " and name \"" << name
                      << std::endl;
            return 0;
         }
         // Create a temporary ElementLink:
         ElementLink< DataVector< OBJECT > > link( linkColKeys()[ i ],
                                                   linkColIndices()[ i ] );
         if( ! link.isValid() ) {
            return 0;
         }
         // Get the pointer:
         return *link;
      }

      // There was an internal error. :-(
      std::cerr << "xAOD::TrigComposite_v1::object ERROR Internal error "
                << "detected" << std::endl;
      return 0;
   }

  template< class CONTAINER >
  bool
  TrigComposite_v1::addObjectCollectionLink( const std::string& collectionName,
                                             const ElementLink< CONTAINER >& link ) {

    // No method currently provided to update or check for uniqueness of a link
    // being added to a container.

    // Add a new object:
    const std::string mangledName = collectionName + s_collectionSuffix;
    linkColNamesNC().push_back( mangledName );
    linkColKeysNC().push_back( link.key() );
    linkColIndicesNC().push_back( link.index() );
    linkColClidsNC().push_back( ClassID_traits< CONTAINER >::ID() );
    return true;
  }

  template< class CONTAINER >
  bool
  TrigComposite_v1::addObjectCollectionLinks( const std::string& collectionName,
                                              const ElementLinkVector< CONTAINER >& links ) {
    // Add all links
    for (const ElementLink< CONTAINER >& link : links ) {
      addObjectCollectionLink( collectionName, link );
    }
    return true;
  }

  template< class CONTAINER >
  ElementLinkVector< CONTAINER >
  TrigComposite_v1::objectCollectionLinks( const std::string& collectionName ) const {
    ElementLinkVector< CONTAINER > links;
    const std::string mangledName = collectionName + s_collectionSuffix;

    const std::vector< std::string >& names = linkColNames();
    for( size_t i = 0; i < names.size(); ++i ) {
       if( names[ i ] != mangledName ) continue;
       // Check that it is of the right type:
       if( linkColClids()[ i ] != ClassID_traits< CONTAINER >::ID() ) {
          const std::string typeName =
             SG::normalizedTypeinfoName( typeid( CONTAINER ) );
          throw std::runtime_error( "xAOD::TrigComposite::objectCollectionLinks: "
                                    "Wrong type (" + typeName + ") requested "
                                    "for collection name \"" + collectionName + "\"" );
       }
       // Construct and add the link:
       links.push_back( ElementLink< CONTAINER >(linkColKeys()[ i ],
                                                 linkColIndices()[ i ]) );
     }
     return std::move(links);
  }

} // namespace xAOD

#endif // XAODTRIGGER_VERSIONS_TRIGCOMPOSITE_V1_ICC
