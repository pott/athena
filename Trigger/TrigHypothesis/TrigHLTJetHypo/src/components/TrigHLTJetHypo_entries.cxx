
#include "TrigHLTJetHypo/TrigHLTJetHypo2.h"
#include "TrigHLTJetHypo/TrigEFRazorAllTE.h"
#include "TrigHLTJetHypo/TrigEFDPhiMetJetAllTE.h"
// #include "TrigHLTJetHypo/TrigHLTJetHypo_HT.h"


#include "../TrigJetHypoAlgMT.h"
#include "../TrigJetHypoToolConfig_EtaEt.h"
#include "../TrigJetHypoToolMT.h"

DECLARE_COMPONENT( TrigHLTJetHypo2 )

DECLARE_COMPONENT( TrigEFRazorAllTE )
DECLARE_COMPONENT( TrigEFDPhiMetJetAllTE )

DECLARE_COMPONENT(TrigJetHypoAlgMT)
DECLARE_COMPONENT(TrigJetHypoToolMT)
DECLARE_COMPONENT(TrigJetHypoToolConfig_EtaEt)
